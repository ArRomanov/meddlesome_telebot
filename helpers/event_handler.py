import re

from helpers import work_with_file


def send_answer_on_template(bot, message):
    list_of_pairs = work_with_file.read_file_with_templates()
    message_text = message.text.lower()
    for key in list_of_pairs:
        template = re.compile(key)
        if template.search(message_text) != None:
            bot.send_message(message.chat.id, list_of_pairs.get(key))


def is_key_in_template(text):
    dict_with_templ = work_with_file.read_file_with_templates()
    for key in dict_with_templ:
        template = re.compile(key)
        if template.search(text) != None:
            return True
