import ast
from helpers import event_handler

path_to_templates = './resources/templates.txt'
path_to_command_start = './resources/command_start.txt'
path_to_command_help = './resources/command_help.txt'


def read_file_with_templates():
    content = read_file(path_to_templates)
    dict_with_temp = ast.literal_eval('{' + content + '}')
    return dict_with_temp


def read_command_start_file():
    content = read_file(path_to_command_start)
    return content


def read_command_help_file():
    content = read_file(path_to_command_help)
    return content


def add_to_templates(template_text):
    try:
        key, value = template_text.split(':')
    except ValueError:
        return 'Ты прислал какую-то хрень, попробуй ввести команду заново'
    if event_handler.is_key_in_template(key):
        return 'Такой шаблон уже сужествует, иди нах'
    else:
        try:
            file = open(path_to_templates, 'a', -1, 'utf8')
            file.write(',\n\'' + key.strip().lower() + '\':\'' + value.strip() + '\'')
            return 'Заебись, можно попробовать'
        except IOError:
            print("Problem opening or writing file")
            return 'Что-то сломалось, сейчас наверное не получится, сорян('
        finally:
            file.close()


def read_file(path_to_file):
    try:
        file = open(path_to_file, 'r', -1, 'utf8')
        content = file.read()
        return content
    except IOError:
        print("Problem opening or reading file")
    finally:
        file.close()
