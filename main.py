import telebot

import security_info as sec
from helpers import event_handler
from helpers import work_with_file

bot = telebot.TeleBot(sec.TOKEN_FOR_SAPUNOVBOT)


@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id, work_with_file.read_command_start_file())


@bot.message_handler(commands=['help'])
def help(message):
    bot.send_message(message.chat.id, work_with_file.read_command_help_file())


@bot.message_handler(commands=['add'])
def add(message):
    sent = bot.send_message(message.chat.id, 'Введи фразу или слово, на которые я должен буду реагировать\n' +
                            'формат - <слово/фраза в чате:мой ответ>')
    bot.register_next_step_handler(sent, input_template)


def input_template(message):
    answer = work_with_file.add_to_templates(message.text)
    bot.send_message(message.chat.id, answer)


@bot.message_handler(content_types=['text'])
def answer_all_message(message):
    event_handler.send_answer_on_template(bot, message)

bot.polling(none_stop=True)
